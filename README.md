A simple, small and ready-to-use Docker image for the Godot game engine (server
edition).

Export templates are included and all images are tagged with the Godot version
and a revision number.

Built automatically using GitLab CI and published to [Docker
Hub](https://hub.docker.com/r/mazechazer/godot/)
