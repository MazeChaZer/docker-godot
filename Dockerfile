FROM debian:stretch-slim

MAINTAINER Jonas Schürmann <jonasschuermann@aol.de>

COPY Godot_v3.0.4-stable_linux_server.64 /usr/local/bin/godot

RUN apt-get update \
    && mkdir -p "/root/.local/share/godot/templates/3.0.4.stable" "/root/.cache" "/root/.config" \
    && apt-get install -y wget unzip \
    && wget --no-verbose "https://downloads.tuxfamily.org/godotengine/3.0.4/Godot_v3.0.4-stable_export_templates.tpz" \
    && unzip Godot_v3.0.4-stable_export_templates.tpz \
    && mv templates/* "/root/.local/share/godot/templates/3.0.4.stable/" \
    && rm Godot_v3.0.4-stable_export_templates.tpz \
    && rmdir templates/ \
    && apt-get purge -y --auto-remove wget unzip \
    && rm -rf /var/lib/apt/lists/*
